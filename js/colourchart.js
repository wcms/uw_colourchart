(function ($) {
  $(function(){
    $('#colourchart').css({
      'font-size' : '86%'
    });
    $('#colourchart, #colourchart th[scope="col"]').css('text-align','center');
    $('#colourchart td').each(function() {
      data = $(this).html();
      if (data.substr(0,1) == '#') {
        newdata = data+'<div></div>';
        th = $(this).closest('table').find('tr:nth-child(2) th').eq($(this).index()).html();
        classes = '';
        if (th == 'links') {
          newdata += '<div class="cclink">example</div>';
        } else if (th == 'link') {
          newdata += '<div class="cclink ccbanner">example</div>';
        } else if (th.substr(0,9) == 'paginator') {
          newdata += '<div class="cclink ccpaginator"><span>1</span><span>2</span><span>3</span></div>';
        } else if (th.substr(0,13) == 'new paginator') {
          newdata += '<div class="cclink ccnewpaginator"><span>1</span><span>2</span><span>3</span></div>';
        }
        if (classes) {
          newdata += '<div class="' + classes + '">example</div>';
        }
        $(this).html(newdata);
        $('div:first',$(this)).css({
          'background-color' : data,
          'width' : '64px',
          'height' : '20px',
          'margin' : '0 auto'
        })
        $('.cclink',$(this)).css({
          'color' : data,
          'text-decoration' : 'underline',
          'margin-top' : '10px',
          'padding' : '2px'
        })
        $('.ccbanner',$(this)).css({
          'background-color' : '#111',
          'font-weight' : 'bold',
          'text-decoration' : 'none',
        })
        $('.ccpaginator',$(this)).css({
          'text-decoration' : 'none'
        })
        $('.ccpaginator span',$(this)).css({
          'background-color' : '#111',
          'margin' : '0 2px',
          'padding' : '2px 4px'
        })
        $('.ccpaginator span:not(:nth-child(2))',$(this)).css({
          'color' : '#FFF'
        })
        $('.ccnewpaginator',$(this)).css({
          'text-decoration' : 'none',
          'margin-bottom' : '-2px'
        })
        $('.ccnewpaginator span',$(this)).css({
          'background-color' : '#111',
          'margin' : '0 1px',
          'padding' : '2px 4px',
          'color' : '#FFF',
          'border' : '1px solid #FFF',
          'display' : 'inline-block',
          'position' : 'relative',
          'top' : '-2px'
        })
        $('.ccnewpaginator span:nth-child(2)',$(this)).css({
          'background-color' : data,
          'outline' : '1px solid' + data,
          'margin' : '0 2px'
        })
      }
    });
  });
})(jQuery);
